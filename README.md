<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!-- <a href="https://https://gitlab.com/Th3Ph4nt0m/mensa-discord/">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

<h3 align="center">Discord Mensa Bot</h3>

  <p align="center">
    A discord bot built around the <a href="https://openmensa.org/">openmensa.org</a> api.
    <br />
    <a href="https://gitlab.com/Th3Ph4nt0m/mensa-discord/-/wikis/home"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://discord.com/api/oauth2/authorize?client_id=1065935580900376626&permissions=2147502080&scope=bot">Invite the bot</a>
    ·
    <a href="https://gitlab.com/Th3Ph4nt0m/mensa-discord/-/issues/new">Open Issue</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
An unofficial Discord bot for [openmensa.org](https://openmensa.org) to get your meal plan directly over Discord.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

* [![Go][Golang]][Go-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To run a local copy for development, follow these steps:

### Prerequisites

* Have [Golang][Go-url] installed
* [Doppler](https://doppler.com/) account
* Have [doppler set up locally](https://docs.doppler.com/docs/install-cli#local-development)
* [Discord](https://discord.com/) account

### Installation

1. Get a free Discord bot Key at the [Discord developer portal](http://discord.com/developers/applications)
2. Enter the API key in your doppler project
3. Clone the repo
   ```sh
   git clone https://gitlab.com/Th3Ph4nt0m/mensa-discord.git
   ```
4. Install NPM packages
   ```sh
   go get
   ```
5. Run the project
   ```sh
   doppler run go run main.go
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- ROADMAP -->
## Roadmap

The first version of this project just supports one Canteen ("Mensa Eupener Straße"). The following version will (as of now) support all openmensa-canteens.

See the [open issues](https://gitlab.com/Th3Ph4nt0m/mensa-discord/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a merge request. You can also simply open an issue.
Don't forget to give the project a star!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feat/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

A license for this project will be decided within the next few months.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Henrik Steffens - me@henrik.tech

Project Link: [https://gitlab.com/Th3Ph4nt0m/mensa-discord/](https://gitlab.com/Th3Ph4nt0m/mensa-discord/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/th3ph4nt0m/mensa-discord.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/Th3Ph4nt0m/mensa-discord/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/th3ph4nt0m/mensa-discord.svg?style=for-the-badge
[forks-url]: https://gitlab.com/Th3Ph4nt0m/mensa-discord/network/members
[stars-shield]: https://img.shields.io/gitlab/stars/th3ph4nt0m/mensa-discord.svg?style=for-the-badge
[stars-url]: https://gitlab.com/Th3Ph4nt0m/mensa-discord/stargazers
[issues-shield]: https://img.shields.io/gitlab/issues/open/th3ph4nt0m/mensa-discord.svg?style=for-the-badge
[issues-url]: https://gitlab.com/Th3Ph4nt0m/mensa-discord/issues
[license-shield]: https://img.shields.io/gitlab/license/th3ph4nt0m/mensa-discord.svg?style=for-the-badge
[license-url]: https://gitlab.com/Th3Ph4nt0m/mensa-discord/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/henrik-steffens
[product-screenshot]: images/screenshot.png
[Golang]: https://img.shields.io/badge/go-00A29C?style=for-the-badge&logo=go&logoColor=white
[Go-url]: https://golang.org/